package com.university.project.web.api;

import com.university.project.model.RetetaDTO;
import com.university.project.persistance.Farmacie.RetetaDAO;
import com.university.project.service.Farmacie.RetetaService;
import com.university.project.service.ServiceModule;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/reteta")
@Import(ServiceModule.class)
@Controller
public class RetetaAPI {

    private final RetetaService retetaService;

    @Autowired
    public RetetaAPI(RetetaService retetaService) {
        this.retetaService = retetaService;
    }

    @GetMapping
    List<RetetaDTO> getRetete(){
        return retetaService.getRetete();
    }


    @GetMapping("{id}/")
    RetetaDTO getReteta(@PathVariable("id") Long id){
        return retetaService.getReteta(id);
    }

    @PostMapping("{id}")
    RetetaDTO addReteta(@PathVariable("id") Long id, @RequestBody RetetaDTO reteta){
        return retetaService.addReteta(id,reteta);
    }

    @PutMapping("{id}/{idm}")
    RetetaDTO updateReteta(@PathVariable("id") Long id, @PathVariable("id") Long idm, @RequestBody RetetaDTO reteta) throws NotFoundException {
        return retetaService.updateReteta(id, idm, reteta);
    }

    @DeleteMapping("{id}")
    void deleteReteta(@PathVariable(value = "id") Long id) throws NotFoundException {
        retetaService.delete(id);
    }
}
