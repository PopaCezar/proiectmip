package com.university.project.web.api;

import com.university.project.model.ClientDTO;
import com.university.project.persistance.Farmacie.ClientDAO;
import com.university.project.service.Farmacie.ClientService;
import com.university.project.service.ServiceModule;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/client")
@Import(ServiceModule.class)
@Controller
public class ClientAPI {

    private final ClientService clientService;

    @Autowired
    public ClientAPI(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    List<ClientDTO> getClients(){
        return clientService.getClients();
    }


    @GetMapping("{id}/")
    ClientDTO getClient(@PathVariable("id") Long id){
        return clientService.getClient(id);
    }

    @PostMapping
    ClientDTO addClient(@RequestBody ClientDTO client){
        return clientService.addClient(client);
    }

    @PutMapping("{id}/")
    ClientDTO updateClient(@PathVariable("id") Long id, @RequestBody ClientDTO client) throws NotFoundException {
        return clientService.updateClient(id, client);
    }

    @DeleteMapping("{id}")
    void deleteClient(@PathVariable(value = "id") Long id) throws NotFoundException {
        clientService.delete(id);
    }
}
