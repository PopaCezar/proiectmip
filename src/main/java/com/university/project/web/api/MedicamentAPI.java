package com.university.project.web.api;

import com.university.project.model.MedicamentDTO;
import com.university.project.persistance.Farmacie.MedicamentDAO;
import com.university.project.service.Farmacie.MedicamentService;
import com.university.project.service.ServiceModule;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/medicament")
@Import(ServiceModule.class)
@Controller
public class MedicamentAPI {

    private final MedicamentService medicamentService;

    @Autowired
    public MedicamentAPI(MedicamentService medicamentService) {
        this.medicamentService = medicamentService;
    }

    @GetMapping
    List<MedicamentDTO> getMedicaments(){
        return medicamentService.getMedicaments();
    }


    @GetMapping("{id}/")
    MedicamentDTO getMedicament(@PathVariable("id") Long id){
        return medicamentService.getMedicament(id);
    }

    @PostMapping
    MedicamentDTO addMedicament(@RequestBody MedicamentDTO medicament){
        return medicamentService.addMedicament(medicament);
    }

    @PutMapping("{id}/")
    MedicamentDTO updateMedicament(@PathVariable("id") Long id, @RequestBody MedicamentDTO medicament) throws NotFoundException {
        return medicamentService.updateMedicament(id, medicament);
    }

    @DeleteMapping("{id}")
    void deleteMedicament(@PathVariable(value = "id") Long id) throws NotFoundException {
        medicamentService.delete(id);
    }
}
