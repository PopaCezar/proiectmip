package com.university.project.web.api;

import com.university.project.model.ChitantaDTO;
import com.university.project.persistance.Farmacie.ChitantaDAO;
import com.university.project.service.Farmacie.ChitantaService;
import com.university.project.service.ServiceModule;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/chitanta")
@Import(ServiceModule.class)
@Controller
public class ChitantaAPI {

    private final ChitantaService chitantaService;

    @Autowired
    public ChitantaAPI(ChitantaService clientService) {
        this.chitantaService = clientService;
    }

    @GetMapping
    List<ChitantaDTO> getChitante(){
        return chitantaService.getChitante();
    }

    @GetMapping("{id}/")
    ChitantaDTO getChitanta(@PathVariable("id") Long id){
        return chitantaService.getChitanta(id);
    }

    @PostMapping("{idc}/{idr}")
    ChitantaDTO addChitanta(@PathVariable("idc") Long idc, @PathVariable("idr") Long idr,
                            @RequestBody ChitantaDTO chitanta){
        return chitantaService.addChitanta(idc,idr,chitanta);
    }

    @PutMapping("{id}/{idc}/{idr}")
    ChitantaDTO updateChitanta(@PathVariable("id") Long id,
                               @PathVariable("idc") Long idc,
                               @PathVariable("idr") Long idr,
                               @RequestBody ChitantaDTO chitanta) throws NotFoundException {
        return chitantaService.updateChitanta(id, idc, idr, chitanta);
    }

    @DeleteMapping("{id}")
    void deleteChitanta(@PathVariable(value = "id") Long id) throws NotFoundException {
        chitantaService.delete(id);
    }
}
