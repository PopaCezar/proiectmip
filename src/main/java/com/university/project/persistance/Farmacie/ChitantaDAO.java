package com.university.project.persistance.Farmacie;

import com.university.project.model.ChitantaDTO;

import javax.persistence.*;

@Entity(name = "Chitanta")
//@Table(name = "chitanta")
public class ChitantaDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    //@MapsId
    ClientDAO client;

    @OneToOne(fetch = FetchType.LAZY)
    //@MapsId
    RetetaDAO reteta;

    private Long pretTotal;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public ClientDAO getClient() {
        return client;
    }

    public void setClient(ClientDAO Client) {
        this.client = Client;
    }

    public RetetaDAO getReteta() {
        return reteta;
    }

    public void setReteta(RetetaDAO Reteta) {
        this.reteta = Reteta;
    }

    public Long getPretTotal() {
        return pretTotal;
    }

    public void setPretTotal(Long pretTotal) {
        this.pretTotal = pretTotal;
    }

    public ChitantaDTO Convert()
    {
       ChitantaDTO chitanta =  new ChitantaDTO();
       chitanta.setId(id);
       chitanta.setPretTotal(pretTotal);
       if(client != null)
           chitanta.setClient(client.Convert());
       if(reteta != null)
           chitanta.setReteta(reteta.Convert());
       return chitanta;

    }

}
