package com.university.project.persistance.Farmacie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Interface that defines the repository for client table
 */
public interface ClientJPA extends JpaRepository<ClientDAO, Long> {


    @Query(value = "select c from Client c")
    List<ClientDAO> getAllClients();

}
