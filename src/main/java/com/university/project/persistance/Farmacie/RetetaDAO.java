package com.university.project.persistance.Farmacie;

import com.university.project.model.RetetaDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Reteta")
//@Table(name = "reteta")
public class RetetaDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String type;

    @OneToOne(fetch = FetchType.LAZY)
    //@MapsId
    private MedicamentDAO medicament;

    @OneToOne(mappedBy = "reteta", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private ChitantaDAO implicatedChitanta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() { return type; }

    public void setType(String type) {
        this.type = type;
    }

    public MedicamentDAO getMedicament() { return medicament; }

    public void setMedicament(MedicamentDAO medicament) { this.medicament = medicament; }

    public ChitantaDAO getImplicatedChitanta() { return implicatedChitanta; }

    public void setImplicatedChitanta(ChitantaDAO implicatedChitanta) { this.implicatedChitanta = implicatedChitanta; }

    public RetetaDTO Convert()
    {
        RetetaDTO reteta = new RetetaDTO();
        reteta.setType(type);
        reteta.setId(id);
        //if(implicatedChitanta != null)
        //    reteta.setChitanta(implicatedChitanta.Convert());
        if(medicament != null)
            reteta.setMedicament(medicament.Convert());
        return reteta;
    }
}
