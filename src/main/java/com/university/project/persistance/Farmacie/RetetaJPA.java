package com.university.project.persistance.Farmacie;

import com.university.project.model.RetetaDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface RetetaJPA extends  JpaRepository<RetetaDAO, Long> {
    @Query(value = "select r from Reteta r")
    List<RetetaDAO> getAllRetete();
}
