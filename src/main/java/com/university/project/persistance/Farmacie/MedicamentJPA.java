package com.university.project.persistance.Farmacie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface MedicamentJPA extends  JpaRepository<MedicamentDAO, Long> {

    @Query(value = "select m from Medicament m")
    List<MedicamentDAO> getAllMedicaments();
}
