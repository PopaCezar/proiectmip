package com.university.project.persistance.Farmacie;

import com.university.project.model.ClientDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Client")
//@Table(name = "client")
public class ClientDAO {

    public ClientDAO(Long id, String name, String status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;
    private String status;

    @OneToOne(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private ChitantaDAO implicatedChitanta;

    public ClientDAO() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public ChitantaDAO getImplicatedChitanta() { return implicatedChitanta; }

    public void setImplicatedChitanta(ChitantaDAO implicatedChitanta) { this.implicatedChitanta = implicatedChitanta; }

    public ClientDTO Convert()
    {
        ClientDTO client = new ClientDTO();
        client.setName(name);
        client.setStatus(status);
        client.setId(id);
        //if(implicatedChitanta != null)
        //    client.setChitanta(implicatedChitanta.Convert());
        return client;
    }
}
