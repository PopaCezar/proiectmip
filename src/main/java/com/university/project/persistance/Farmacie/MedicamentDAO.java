package com.university.project.persistance.Farmacie;

import com.university.project.model.MedicamentDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Medicament")
//@Table(name = "medicament")
public class MedicamentDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)

    private String name;
    private Long price;

    @OneToOne(mappedBy = "medicament", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private RetetaDAO implicatedReteta;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() { return price; }

    public void setPrice(Long price) { this.price = price; }

    public RetetaDAO getImplicatedReteta() { return implicatedReteta; }

    public void setImplicatedReteta(RetetaDAO implicatedReteta) { this.implicatedReteta = implicatedReteta; }

    public MedicamentDTO Convert()
    {
        MedicamentDTO medicament = new MedicamentDTO();
        medicament.setName(name);
        medicament.setId(id);
        medicament.setPrice(price);
        //if(implicatedReteta != null)
        //   medicament.setReteta(implicatedReteta.Convert());
        return medicament;
    }
}
