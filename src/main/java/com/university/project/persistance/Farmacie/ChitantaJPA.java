package com.university.project.persistance.Farmacie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ChitantaJPA extends  JpaRepository<ChitantaDAO, Long> {

    @Query(value = "select c from Chitanta c")
    List<ChitantaDAO> getAllChitante();
}
