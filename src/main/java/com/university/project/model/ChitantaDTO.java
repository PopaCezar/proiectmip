package com.university.project.model;

import com.university.project.persistance.Farmacie.ChitantaDAO;

public class ChitantaDTO {
    private Long id;
    private ClientDTO client;
    private RetetaDTO reteta;
    private Long pretTotal;

    public ChitantaDTO(Long id, ClientDTO client, RetetaDTO reteta, Long pretTotal) {
        this.id = id;
        this.client = client;
        this.reteta = reteta;
        this.pretTotal = pretTotal;
    }

    public ChitantaDTO() {

    }

    public ClientDTO getClient() { return client; }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public RetetaDTO getReteta() {
        return reteta;
    }

    public void setReteta(RetetaDTO reteta) {
        this.reteta = reteta;
    }

    public Long getPretTotal() {
        return pretTotal;
    }

    public void setPretTotal(Long pretTotal) {
        this.pretTotal = pretTotal;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public ChitantaDAO Convert()
    {
        ChitantaDAO chitanta =  new ChitantaDAO();
        chitanta.setId(id);
        chitanta.setPretTotal(pretTotal);
        if(client != null)
            chitanta.setClient(client.Convert());
        if(reteta != null)
            chitanta.setReteta(reteta.Convert());
        return chitanta;

    }
}
