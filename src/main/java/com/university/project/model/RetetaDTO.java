package com.university.project.model;

import com.university.project.persistance.Farmacie.RetetaDAO;

public class RetetaDTO {
    private Long id;
    private String type;
    private MedicamentDTO medicament;
    private ChitantaDTO chitanta;

    public RetetaDTO(Long id, String type, MedicamentDTO medicament, ChitantaDTO chitanta) {
        this.id = id;
        this.type=type;
        this.medicament = medicament;
        this.chitanta = chitanta;
    }

    public RetetaDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public MedicamentDTO getMedicament() { return medicament; }

    public void setMedicament(MedicamentDTO medicament) { this.medicament = medicament; }

    public ChitantaDTO getChitanta() { return chitanta; }

    public void setChitanta(ChitantaDTO chitanta) { this.chitanta = chitanta; }

    public RetetaDAO Convert()
    {
        RetetaDAO reteta = new RetetaDAO();
        reteta.setType(type);
        reteta.setId(id);
        //if(chitanta!= null)
        //    reteta.setImplicatedChitanta(chitanta.Convert());
        if(medicament != null)
            reteta.setMedicament(medicament.Convert());
        return reteta;
    }
}

