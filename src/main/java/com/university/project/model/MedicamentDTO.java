package com.university.project.model;

import com.university.project.persistance.Farmacie.MedicamentDAO;

public class MedicamentDTO {
    private Long id;
    private String name;
    private Long price;
    private RetetaDTO reteta;

    public MedicamentDTO(Long id, String name, Long price, RetetaDTO reteta) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.reteta = reteta;
    }

    public MedicamentDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() { return this.name; }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() { return price; }

    public void setPrice(Long price) { this.price = price; }

    public RetetaDTO getReteta() { return reteta; }

    public void setReteta(RetetaDTO reteta) { this.reteta = reteta; }

    public MedicamentDAO Convert()
    {
        MedicamentDAO medicament = new MedicamentDAO();
        medicament.setName(name);
        medicament.setId(id);
        medicament.setPrice(price);
        //if(reteta!= null)
        //    medicament.setImplicatedReteta(reteta.Convert());
        return medicament;
    }
}
