package com.university.project.model;

import com.university.project.persistance.Farmacie.ClientDAO;

public class ClientDTO {
        private Long id;
        private String name;
        private String status;
        private ChitantaDTO chitanta;

        public ClientDTO(Long id, String name, String status, ChitantaDTO chitanta) {
            this.id = id;
            this.name = name;
            this.status = status;
            this.chitanta = chitanta;
        }

    public ClientDTO() {

    }

    public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() { return status; }

        public void setStatus(String status) { this.status = status; }

    public ChitantaDTO getChitanta() { return chitanta; }

    public void setChitanta(ChitantaDTO chitanta) { this.chitanta = chitanta; }

    public ClientDAO Convert()
    {
        ClientDAO client = new ClientDAO();
        client.setName(name);
        client.setStatus(status);
        client.setId(id);
        //if(chitanta != null)
        //    client.setImplicatedChitanta(chitanta.Convert());
        return client;
    }
}

