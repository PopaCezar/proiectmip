package com.university.project.service;

import com.university.project.persistance.PersistenceModule;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * Class used for configuring the service package in spring container
 */
@Configuration
@ComponentScan
@Import(PersistenceModule.class)
public class ServiceModule {
}
