package com.university.project.service.Farmacie;
import com.university.project.model.MedicamentDTO;
import com.university.project.persistance.Farmacie.MedicamentDAO;
import com.university.project.persistance.Farmacie.MedicamentJPA;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicamentService {
    private final MedicamentJPA medicamentJPA;

    @Autowired
    public MedicamentService(MedicamentJPA medicamentJPA) {
        this.medicamentJPA = medicamentJPA;
    }


    public List<MedicamentDTO> getMedicaments() {
        return medicamentJPA
                .getAllMedicaments()
                .stream()
                .map(medicament -> medicament.Convert())
                .collect(Collectors.toList());
    }

    public MedicamentDTO addMedicament(MedicamentDTO medicament) {
       MedicamentDAO med = new MedicamentDAO();
        med.setName(medicament.getName());
        med.setPrice(medicament.getPrice());
        //med.setImplicatedReteta(medicament.getReteta().Convert());
        MedicamentDAO savedMedicament = medicamentJPA.save(med);
        return savedMedicament.Convert();
    }

    public MedicamentDTO getMedicament(Long id) {
        MedicamentDAO medicament = medicamentJPA.getOne(id);
        return medicament.Convert();
    }

    public MedicamentDTO updateMedicament(Long id, MedicamentDTO medicament) throws NotFoundException {
        MedicamentDAO med = medicamentJPA.getOne(id);
        if (med == null) {
            throw new NotFoundException("Medicament not Found");
        }
        med.setName(medicament.getName());
        med.setPrice(medicament.getPrice());
        //med.setImplicatedReteta(medicament.getReteta().Convert());
        MedicamentDAO savedMedicament = medicamentJPA.save(med);
        return savedMedicament.Convert();
    }

    public void delete(Long id) throws NotFoundException {
        MedicamentDAO medicament = medicamentJPA.getOne(id);
        if (medicament == null) {
            throw new NotFoundException("Medicament not Found");
        }
        medicamentJPA.delete(medicament);
    }

}
