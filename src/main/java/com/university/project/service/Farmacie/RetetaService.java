package com.university.project.service.Farmacie;

import com.university.project.model.ChitantaDTO;
import com.university.project.model.RetetaDTO;
import com.university.project.persistance.Farmacie.MedicamentDAO;
import com.university.project.persistance.Farmacie.MedicamentJPA;
import com.university.project.persistance.Farmacie.RetetaDAO;
import com.university.project.persistance.Farmacie.RetetaJPA;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RetetaService {
    private final RetetaJPA retetaJPA;
    private final MedicamentJPA medicamentJPA;
    @Autowired
    public RetetaService(RetetaJPA retetaJPA, MedicamentJPA medicamentJPA) {
        this.retetaJPA = retetaJPA;
        this.medicamentJPA = medicamentJPA;
    }

    public RetetaJPA getRetetaJPA() {
        return retetaJPA;
    }

    public MedicamentJPA getMedicamentJPA() {
        return medicamentJPA;
    }

    public List<RetetaDTO> getRetete() {
        return retetaJPA
                .getAllRetete()
                .stream()
                .map(reteta -> reteta.Convert())
                .collect(Collectors.toList());
    }

    public RetetaDTO addReteta(Long id,RetetaDTO reteta) {
        RetetaDAO ret = new RetetaDAO();
        ret.setType(reteta.getType());
        MedicamentDAO medicament = medicamentJPA.getOne(id);
        ret.setMedicament(medicament);
        //ret.setImplicatedChitanta(reteta.getChitanta().Convert());
        RetetaDAO savedReteta = retetaJPA.save(ret);
        return savedReteta.Convert();
    }

    public RetetaDTO getReteta(Long id) {
        RetetaDAO reteta = retetaJPA.getOne(id);
        return reteta.Convert();
    }

    public RetetaDTO updateReteta(Long id, Long idm ,RetetaDTO reteta) throws NotFoundException {
       RetetaDAO ret = retetaJPA.getOne(id);
        if (ret == null) {
            throw new NotFoundException("Reteta not Found");
        }
        ret.setType(reteta.getType());
        MedicamentDAO medicament = medicamentJPA.getOne(id);
        ret.setMedicament(medicament);
        //ret.setMedicament(reteta.getMedicament().Convert());
        //ret.setImplicatedChitanta(reteta.getChitanta().Convert());
        RetetaDAO savedReteta = retetaJPA.save(ret);
        return savedReteta.Convert();
    }

    public void delete(Long id) throws NotFoundException {
        RetetaDAO reteta = retetaJPA.getOne(id);
        if (reteta == null) {
            throw new NotFoundException("Reteta not Found");
        }
        retetaJPA.delete(reteta);
    }

}