package com.university.project.service.Farmacie;

import com.university.project.model.ChitantaDTO;
import com.university.project.persistance.Farmacie.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChitantaService {
    private final ChitantaJPA chitantaJPA;
    private final RetetaJPA retetaJPA;
    private final ClientJPA clientJPA;

    @Autowired
    public ChitantaService(ChitantaJPA chitantaJPA, RetetaJPA retetaJPA, ClientJPA clientJPA) {
        this.chitantaJPA = chitantaJPA;
        this.retetaJPA = retetaJPA;
        this.clientJPA = clientJPA;
    }

    public RetetaJPA getRetetaJPA() {
        return retetaJPA;
    }

    public ClientJPA getClientJPA() {
        return clientJPA;
    }

    public ChitantaJPA getChitantaJPA() {
        return chitantaJPA;
    }

    public List<ChitantaDTO> getChitante() {
        return chitantaJPA
                .getAllChitante()
                .stream()
                .map(chitanta -> chitanta.Convert())
                .collect(Collectors.toList());
    }

    public ChitantaDTO addChitanta(Long idc, Long idr, ChitantaDTO chitanta) {
        ChitantaDAO chit = new ChitantaDAO();
        ClientDAO client = clientJPA.getOne(idc);
        RetetaDAO reteta = retetaJPA.getOne(idr);
        chit.setClient(client);
        chit.setReteta(reteta);
        //chit.setClient(chitanta.getClient().Convert());
        //chit.setReteta(chitanta.getReteta().Convert());
        chit.setPretTotal(chitanta.getPretTotal());
        ChitantaDAO savedChitanta = chitantaJPA.save(chit);
        return savedChitanta.Convert();
    }

    public ChitantaDTO getChitanta(Long id) {
        ChitantaDAO chitanta = chitantaJPA.getOne(id);
        return chitanta.Convert();
    }

    public ChitantaDTO updateChitanta(Long id, Long idc, Long idr, ChitantaDTO chitanta) throws NotFoundException {
        ChitantaDAO chit = chitantaJPA.getOne(id);
       if (chit == null) {
            throw new NotFoundException("Chitanta not Found");
        }
        ClientDAO client = clientJPA.getOne(idc);
        RetetaDAO reteta = retetaJPA.getOne(idr);
        chit.setClient(client);
        chit.setReteta(reteta);
        chit.setPretTotal(chitanta.getPretTotal());
        ChitantaDAO savedChitanta = chitantaJPA.save(chit);
        return savedChitanta.Convert();
    }

    public void delete(Long id) throws NotFoundException {
        ChitantaDAO chitanta = chitantaJPA.getOne(id);
        if (chitanta == null) {
            throw new NotFoundException("Chitanta not Found");
        }
        chitantaJPA.delete(chitanta);
    }
}