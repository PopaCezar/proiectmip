package com.university.project.service.Farmacie;


import com.university.project.model.ClientDTO;
import com.university.project.persistance.Farmacie.ClientDAO;
import com.university.project.persistance.Farmacie.ClientJPA;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {
    private final ClientJPA clientJPA;

    @Autowired
    public ClientService(ClientJPA clientJPA) {
        this.clientJPA = clientJPA;
    }


    public List<ClientDTO> getClients() {
        return clientJPA
                .getAllClients()
                .stream()
                .map(client -> client.Convert())
                .collect(Collectors.toList());
    }

    public ClientDTO addClient(ClientDTO client) {
        ClientDAO cli = new ClientDAO();
        cli.setName(client.getName());
        cli.setStatus(client.getStatus());
        //cli.setImplicatedChitanta(client.getChitanta().Convert());
        ClientDAO savedClient = clientJPA.save(cli);
        return savedClient.Convert();
    }

    public ClientDTO getClient(Long id) {
        ClientDAO client = clientJPA.getOne(id);
        return client.Convert();
    }

    public ClientDTO updateClient(Long id, ClientDTO client) throws NotFoundException {
        ClientDAO cli = clientJPA.getOne(id);
        if (cli == null) {
            throw new NotFoundException("Client not Found");
        }
        cli.setName(client.getName());
        cli.setStatus(client.getStatus());
        //cli.setImplicatedChitanta(client.getChitanta().Convert());
        ClientDAO savedClient = clientJPA.save(cli);
        return savedClient.Convert();
    }

    public void delete(Long id) throws NotFoundException {
        ClientDAO client = clientJPA.getOne(id);
        if (client == null) {
            throw new NotFoundException("Client not Found");
        }
        clientJPA.delete(client);
    }
}
